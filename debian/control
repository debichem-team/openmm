Source: openmm
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders:
 Robert McGibbon <rmcgibbo@gmail.com>,
 Andreas Tille <tille@debian.org>,
 Andrius Merkys <merkys@debian.org>,
Section: libs
Priority: optional
Build-Depends:
 chrpath,
 cmake,
 cython3,
 debhelper-compat (= 13),
 dh-python,
 doxygen,
 dpkg-dev (>= 1.22.5),
 libsimde-dev,
 ocl-icd-opencl-dev,
 python3-all-dev:any,
 python3-numpy,
 python3-pytest <!nocheck>,
 python3-pytest-xdist <!nocheck>,
 python3-scipy <!nocheck>,
 python3-setuptools,
 swig,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debichem-team/openmm
Vcs-Git: https://salsa.debian.org/debichem-team/openmm.git
Homepage: https://simtk.org/projects/openmm

Package: libopenmm-dev
Architecture: any
Section: libdevel
Depends:
 libopenmm8.1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: C++ header files for the OpenMM library
 OpenMM is a software toolkit for performing molecular simulations on a range
 of high performance computing architectures. This package provides C++ header
 files for the development with that library.

Package: libopenmm-plugins
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Plugins for the OpenMM library
 OpenMM is a software toolkit for performing molecular simulations on a range
 of high performance computing architectures. This package provides OpenMM
 plugins.

Package: libopenmm8.1
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: High-performance molecular simulation library
 OpenMM is a software toolkit for performing molecular simulations on a range
 of high performance computing architectures. It is based on a layered
 architecture: the lower layers function as a reusable library that can be
 invoked by any application, while the upper layers form a complete environment
 for running molecular simulations.

Package: python3-openmm
Architecture: any
Section: python
Depends:
 libopenmm-dev,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Provides:
 python3-simtk,
 ${python3:Provides},
Breaks:
 python3-simtk,
Replaces:
 python3-simtk,
X-Python3-Version: ${python3:Versions}
Description: Python bindings for the OpenMM molecular simulation package
 OpenMM is a software toolkit for performing molecular simulations on a range
 of high performance computing architectures. This package provides the
 Python application layer for the package.
